# -*- coding: utf-8 -*-

from odoo import models, fields, api

from fhirclient.models.practitioner import Practitioner
import re

class doctor_fhir(models.Model):
  _inherit = 'doctor.doctor'

  practitionerId = fields.Char(string='Practitioner ID', readonly=True)

  def getGivenName(self, name):
    return name.split()[:-1]

  def getLastName(self, name):
    return name.split()[-1]

  def createHumanName(self, use, name):
    lastName = self.getLastName(name)
    givenName = self.getGivenName(name)

    return { "use": use, "given": givenName, "family": lastName } 

  def createAddress(self, use, addressLines, city, postalCode, state, country):
    return {
      "use": use,
      "line": addressLines.splitlines(),
      "city": city,
      "postalCode": postalCode,
      "state": state,
      "country": country
    }

  def createPractitioner(self, names, addresses):
    return { "active": True, "name": names, "address": addresses }

  def createFhirPractitioner(self, practitionerDict):
    client = self.env['fhir_config.config'].getClientInstance()
    practitionerInstance = Practitioner(practitionerDict)
    
    # send to server
    response = Practitioner.create(practitionerInstance, client.server)
    return response

  def getIdFromResponse(self, resource, response):
    diagnostics = response["issue"][0]["diagnostics"]
    match = re.search('(' + resource + '/\d+)/_history/\d+', diagnostics)
    resourceId = match.group(1)
    return resourceId

  @api.model
  def create(self, values):
    record = super(doctor_fhir, self).create(values)

    name = self.createHumanName("official", record["name"])
    homeAddress = self.createAddress(
      "home",
      record["address"],
      record["city"],
      record["postalCode"],
      record["state"],
      record["country"])
    
    practitioner = self.createPractitioner([name], [homeAddress])

    response = self.createFhirPractitioner(practitioner)
    record['practitionerId'] = self.getIdFromResponse("Practitioner", response)

    return record
          
