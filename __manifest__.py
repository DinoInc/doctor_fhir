# -*- coding: utf-8 -*-
{
    'name': "Doctor FHIR",

    'summary': """
        Integration doctor module with FHIR""",

    'description': """
        Integraton doctor module with FHIR for traceability.
    """,

    'author': "eHealth ID",
    'website': "https://ehealth.id",

    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base', 'doctor', 'fhir_config'],

    'data': [
        'views/doctor_fhir.xml'
    ],
}
